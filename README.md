#The Ambient Lighting Plugin
The Ambient Lighting Plugin supports interactions with smart lighting solutions, such as artnet enabled lights, LIFX and Phillips Hue. 

All interaction is performed through the Ambient Control mechanisms. See [Ambient Control documentation](https://bitbucket.org/dynamixdevelopers/ambientcontrolplugin)

The plugins control description is as follows:
```JSON
{
    "name":"LightPlugin",
    "artifact_id":"org.ambientdynamix.contextplugins.hueplugin",
    "description":"",
    "pluginVersion":"1.0.0",
    "owner_id":"",
    "platform":"",
    "minPlatformVersion":"",
    "minFrameworkVersion":"",
    "inputList":[
        {
            "mandatoryControls":[
                "DISPLAY_COLOR",
                "SWITCH"
            ],
            "priority":1
        },
        {
            "mandatoryControls":[
                "DISPLAY_COLOR"
            ],
            "priority":2
        },
        {
            "mandatoryControls":[
                "SWITCH"
            ],
            "priority":3
        }
    ],
    "outputList":{
        "color":"DISPLAY_COLOR"
    },
    "optionalInputList":[]
}
```